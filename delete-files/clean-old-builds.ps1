﻿##############################
#
# Clean old builds and keep some of them.
#
##############################

#cd C:\apps\Tomcat\webapps\cdd-develop

$PREFIX = "2.0.0."
$PATTERN = "^2.0.0.(\d+)$"
$MAX_TO_DELETE = 350
$STEP = 10

$map = @{}

# map the folders according to its number
ls | foreach {
    $folder = $_.Name
	$match = [regex]::Match($folder, $PATTERN)
    if ($match.Success) {
		$number = $match.Groups[1].Value
        $key = "" + [math]::Floor($number / $STEP)
        $folders = $map[$key]
        if ($folders -eq $null) {
            $folders = @()
        }
		
		if ([int]$number -le $MAX_TO_DELETE) {
			$folders += $number
			$map[$key] = $folders
		}
    }
}

#$map

# sort the folders by its number value
$map.Keys | foreach {
    $folders = $map[$_]
    $m = $folders | Measure-Object -Minimum
    $min = $m.Minimum
    $folders | foreach {
        if ($_ -ne $min) {
			$folder = "$PREFIX$_"
            write "removing folder $folder"
            rm "$folder" -Recurse -Force
        }
    }
}
